package edu.fzu;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


/**
 * 
 * @author 谢其钦
 *
 */

public class Score {
	/**
	 * 完成配置文件分数的提取以及调用totalScore计算总分
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// 读取配置文件total.propertises
		// 创建一个配置文件的类
		Properties properties = new Properties();
		try {
			InputStream inputStream = new BufferedInputStream(
					new FileInputStream("total.properties"));
			properties.load(inputStream);
			// 取出元素
			Double before = (double) Integer.parseInt(properties.getProperty("before"));//课前自测总分
			Double base = (double) Integer.parseInt(properties.getProperty("base"));//课堂完成部分
			Double test = (double) Integer.parseInt(properties.getProperty("test"));//小测部分
			Double program = (double) Integer.parseInt(properties.getProperty("program"));//编程题部分
			Double add = (double) Integer.parseInt(properties.getProperty("add"));//附加题
			//计算每个模块的总得分
			Scanner scanner = new Scanner(System.in);
			String small = scanner.next();
			String all =scanner.next();
			scanner.close();
			HashMap<String, Integer> total = new HashMap<>();
			total = getSection_Total(small,all);
			totalScore(total,before,base,test,program,add);
			
		} catch (FileNotFoundException e) {
			System.out.println("文件名不存在或文件名非法");
		}

	}
	
	/**
	 * 
	 * @param total 用于传入每个模块的名字及对应分数
	 * @param before 从配置文件取出的课前自测总分
	 * @param base  从配置文件取出的课堂完成部分
	 * @param test 从配置文件取出的小测部分
	 * @param program 从配置文件取出的编程题部分
	 * @param add 从配置文件取出的附加题
	 */
	public static void totalScore(HashMap<String, Integer> total, Double before, Double base, Double test, Double program, Double add) {
		Double total_score = 0.0;
		//计算课前自测成绩
		int bef = total.get("before");
		Double res_bef = bef/before * 100*0.25;
		//计算课堂完成部分成绩
		int bas = total.get("base");
		Double res_bas = bas/base * 100*0.95*0.3;
		//小测部分
		int tes = total.get("test");
		Double res_tes = tes/test*100*0.25;
		//计算编程部分成绩
		int pro = total.get("program");
		Double res_pro = pro/program*100*0.1;
		if (res_pro>95) {
			res_pro = (double) 95;
		}
		//计算附加题
		int ad = total.get("add");
		Double res_ad = ad/add * 100 * 0.05;
		if (res_ad>90) {
			res_ad = (double) 90;
		}
		total_score = res_ad + res_bas + res_bef + res_pro + res_tes;
		System.out.println(total_score);
	}
/**
 * HashMap<String, Integer> getSection_Total(String small, String all) 此函数用于将两个班课个模块的分数计算
 * @param small 小班课路径
 * @param all 大班课路径
 * @return 为HashMap的allClass 返回对应模块分数总和
 * @throws IOException
 */
	public static HashMap<String, Integer> getSection_Total(String small, String all) throws IOException {
		//形成路径
		String filepath1 = small;
		String filepath2 = all;
		//计算大班小班各模块的分数并返回给相应的集合
		java.util.HashMap<String, Integer> smallClass = getScore(filepath1);
		java.util.HashMap<String, Integer> allClass = getScore(filepath2);
		//遍历两个集合的值算出每个模块总分的总和
		String[]section = new String[5];
		for (int i = 0; i < section.length; i++) {
			section[i] = new String();
		}
		section[0] = "before";
		section[1] = "base";
		section[2] = "test";
		section[3] = "program";
		section[4] = "add";
//		System.out.println(allClass);
		for (int i = 0; i < section.length; i++) {
		    int x = allClass.get(section[i]);
		    x+= smallClass.get(section[i]);
		    allClass.put(section[i], x);
		}
		return allClass;
				
	}

	/**
	 * 此函数用于解析网页获取每个模块的经验值
	 * @param filepath 文件路径
	 * @return 返回每个模块对应的经验值
	 * @throws IOException
	 */
	public static HashMap<String, Integer> getScore(String filepath) throws IOException {
		// 从文件中加载文档
		org.jsoup.nodes.Document document = Jsoup.parse(new File(filepath), "utf-8");
		//定义各个模块
		Integer before = 0;
		Integer base = 0;
		Integer test = 0;
		Integer program =0;
		Integer add = 0;
		String regex1 = "课前自测";
		String regex2 = "课堂完成部分";
		String regex3 = "课堂小测";
		String regex4 = "编程题";
		String regex5 = "附加题";
	    String jinyan_regex = "\\d+";
	    String jinyan = null;
	    int jy = 0;
		// 获取相关元素,interaction-row 包含子元素span(经验)
		Elements interaction_row = document.getElementsByClass("interaction-row");
		for (int i = 0; i < interaction_row.size(); i++) {
			
            Element section = interaction_row.get(i).children().get(1).children().get(0).children().get(1);
			Element span_7 = interaction_row.get(i).children().get(1).children().get(2).children().get(0).children().get(6);//已参与&nbsp
			Element span_8= interaction_row.get(i).children().get(1).children().get(2).children().get(0).children().get(interaction_row.get(i).children().get(1).children().get(2).children().get(0).children().size()-1);//经验值
			Pattern p = Pattern.compile(jinyan_regex); 
			Matcher matcher = p.matcher(span_8.text());
			boolean result = matcher.find();
			if (result) {
				jy = Integer.parseInt(matcher.group(0));
				
			}
			
			
			if (section.text().contains(regex1)) {
				before+=jy;
			}else if (section.text().contains(regex2)) {
				
				base+=jy;
			}else if (section.text().contains(regex3)) {
				if (span_8.toString().contains("互评")) {
					Element span_6= interaction_row.get(i).children().get(1).children().get(2).children().get(0).children().get(interaction_row.get(i).children().get(1).children().get(2).children().get(0).children().size()-3);
					Pattern p1 = Pattern.compile(jinyan_regex); 
					Matcher matcher1 = p1.matcher(span_6.text());
					boolean result1 = matcher1.find();
					if (result1) {
						
						test += Integer.parseInt(matcher.group(0))+jy;	
					}
				}
			}else if (section.text().contains(regex4)) {
				program+=jy;
			}else if (section.text().contains(regex5)) {
				add+=jy;
			}
				
         
		}
	
		//将提取的每个模块的经验值存入Map并返回，便于存取运算
        HashMap<String, Integer> section_jy = new HashMap<>();
        section_jy.put("before", before);
        section_jy.put("base", base);
        section_jy.put("test", test);
        section_jy.put("program", program);
        section_jy.put("add", add);
		return section_jy;
	}
}
